#!/bin/bash
#SBATCH -J Benchmark
#SBATCH -p haswell64
#SBATCH --nodes=20
#SBATCH --ntasks-per-node=24
#SBATCH --mem-per-cpu=1443
#SBATCH --cpus-per-task=1
##SBATCH --exclusive
#SBATCH --mail-user=tome821d@tu-dresden.de
#SBATCH --output /scratch/ws/0/tome821d-ogs_cube_ht_benchmark_rev0/%j-out.txt
#SBATCH --error /scratch/ws/0/tome821d-ogs_cube_ht_benchmark_rev0/%j-err.txt
#SBATCH --time=00:03:00

module purge


module load Ninja
module load git
module load CMake/3.21.1
#module load HDF5/1.12.1 # same as load HDF5/1.12.1-gompi-2021b
# loaded by HDF5 : GCCcore/11.2.0 
module load Python/3.9.6
module load Eigen/3.3.9 Boost/1.77
#module load VTK/8.2.0-foss-2020a-Python-3.8.2
module load OpenMPI/4.1.1
# OpenMPI/4.1.1-GCC-11.2.0 load by HDF5
module load ScaLAPACK/2.1.0-gompi-2021b-fb
# module load GCCcore/11.2.0 load by OpenMPI



# add for PETSc
#LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/tome821d/lib
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/h4/tome821d/w/petsc_v3.16.3/tome821d/lib
mkdir /scratch/ws/0/tome821d-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}

srun /home/tome821d/o/build/rp_cpmhdf/bin/ogs -l debug /home/tome821d/data/fracturedcube/rev2/480/1file.prj -o /scratch/ws/0/tome821d-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}/

mv /scratch/ws/0/tome821d-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}-out.txt /scratch/ws/0/tome821d-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}/out.txt
mv /scratch/ws/0/tome821d-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}-err.txt /scratch/ws/0/tome821d-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}/err.txt

