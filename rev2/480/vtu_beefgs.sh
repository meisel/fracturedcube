#!/bin/bash
#SBATCH -J Benchmark
#SBATCH -p haswell64
#SBATCH --nodes=20
#SBATCH --ntasks-per-node=24
#SBATCH --mem-per-cpu=1443
#SBATCH --cpus-per-task=1
##SBATCH --exclusive
#SBATCH --mail-user=tome821d@tu-dresden.de
#SBATCH --output /beegfs/ws/0/tome821d-ogs_beegfs/%j-out.txt
#SBATCH --error /beegfs/ws/0/tome821d-ogs_beegfs/%j-err.txt
#SBATCH --time=00:03:00



module purge
module load foss/2021b
module load Python/3.9.6
module load HDF5/1.12.1

# add for PETSc
#LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/tome821d/lib
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/tome821d/petsc/haswell/petsc_v3.16.4/arch-linux-c-opt/lib
mkdir /beegfs/ws/0/tome821d-ogs_beegfs/${SLURM_JOB_ID}

srun /home/tome821d/o/build/rp/bin/ogs -l debug /home/tome821d/data/fracturedcube/rev2/480/vtufile.prj -o /beegfs/ws/0/tome821d-ogs_beegfs/${SLURM_JOB_ID}/

mv /beegfs/ws/0/tome821d-ogs_beegfs/${SLURM_JOB_ID}-out.txt /beegfs/ws/0/tome821d-ogs_beegfs/${SLURM_JOB_ID}/out.txt
mv /beegfs/ws/0/tome821d-ogs_beegfs/${SLURM_JOB_ID}-err.txt /beegfs/ws/0/tome821d-ogs_beegfs/${SLURM_JOB_ID}/err.txt

