#!/bin/bash
#SBATCH -J Benchmark
#SBATCH -p haswell64
#SBATCH --nodes=20
#SBATCH --ntasks-per-node=24
#SBATCH --mem-per-cpu=1443
#SBATCH --cpus-per-task=1
##SBATCH --exclusive
#SBATCH --mail-user=tome821d@tu-dresden.de
#SBATCH --output /scratch/ws/0/tome821d-ogs_cube_ht_benchmark_rev0/%j-out.txt
#SBATCH --error /scratch/ws/0/tome821d-ogs_cube_ht_benchmark_rev0/%j-err.txt
#SBATCH --time=00:03:00



module purge
module load foss/2021b
module load Python/3.9.6
module load HDF5/1.12.1

# add for PETSc
#LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/tome821d/lib
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/tome821d/petsc/haswell/petsc_v3.16.4/arch-linux-c-opt/lib
mkdir /scratch/ws/0/tome821d-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}

srun /home/tome821d/o/build/haswell/rp/bin/ogs -l debug /home/tome821d/data/fracturedcube/rev2/480/480files.prj -o /scratch/ws/0/tome821d-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}/

mv /scratch/ws/0/tome821d-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}-out.txt /scratch/ws/0/tome821d-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}/out.txt
mv /scratch/ws/0/tome821d-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}-err.txt /scratch/ws/0/tome821d-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}/err.txt

