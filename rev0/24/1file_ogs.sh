#!/bin/bash
#SBATCH -J Benchmark
#SBATCH -p haswell64
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=24
#SBATCH --mem-per-cpu=1443
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --mail-user=thfi710b@tu-dresden.de
#SBATCH --output /scratch/ws/0/thfi710b-ogs_cube_ht_benchmark_rev0/%j-out.txt
#SBATCH --error /scratch/ws/0/thfi710b-ogs_cube_ht_benchmark_rev0/%j-err.txt
#SBATCH --time=00:10:00

module load VTK/8.2.0-foss-2020a-Python-3.8.2
module load OpenMPI/4.0.3-GCC-9.3.0
module load HDF5/1.10.6-gompi-2020a
# add for PETSc
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/thfi710b/lib

mkdir /scratch/ws/0/thfi710b-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}

srun /home/thfi710b/w/o/build/rp/bin/ogs /home/thfi710b/data/fracturedcube/rev0/24/1file.prj -o /scratch/ws/0/thfi710b-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}/

mv /scratch/ws/0/thfi710b-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}-out.txt /scratch/ws/0/thfi710b-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}/out.txt
mv /scratch/ws/0/thfi710b-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}-err.txt /scratch/ws/0/thfi710b-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}/err.txt

