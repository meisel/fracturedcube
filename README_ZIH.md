# Clone and Build OpenGeoSys

Usually, I create a folder 'w' for the source code I'm working on.
In the following description this is used sometimes.

```bash
~ $ mkdir -p w/o
```

## Checkout the soucres


### Original

Clone the sources within the 'w/o' folder into the sub-folder 's':
```bash
~/w/o $ git clone https://gitlab.opengeosys.org/ogs/ogs.git s
```


## Build the binary

### Preliminary: Build PETSc package on TAURUS

run 
`sh <ogs_sources>/scripts/env/taurus/build_petsc.sh`

### Build OpenGeoSys on TAURUS

edit 
`sh <ogs_sources>/scripts/env/taurus/build_ogs.sh`

and add (for analysis with ScoreP)
-DOGS_USE_PYTHON=OFF

run
`sh <ogs_sources>/scripts/env/taurus/build_ogs.sh`

More information how to build OpenGeoSys can be found here:
https://www.opengeosys.org/docs/devguide/getting-started/introduction/


# Checkout the Example Simulation Model

```bash
~ $ mkdir data
cd data
```

```bash
~/data $ git clone https://git.ufz.de/TobiasMeisel/fracturedcube.git
```
or
```bash
~/data $ git clone git@git.ufz.de:TobiasMeisel/fracturedcube.git
```

# Modify data output

In the prj files under `<time_loop><output>` all variable to be written are listed.

```
           <variables>
                <variable>T</variable>
                <variable>p</variable>
                <variable>darcy_velocity</variable>
           </variables>
```


Removed variables `<variable>x</variable>` will not be written.


# Domain decomposition

1. Adapt partition.sh 

1.1 Provide the location of petsc.sh and user.sh 
```
source $HOME/o/s/scripts/env/taurus/petsc.sh
source $HOME/o/s/scripts/env/taurus/user.sh
```
1.2 Provid the location of the ogs binaries

`BIN_PATH=$HOME/o/build/haswell/rp/bin`
This folder should contain executable for partmesh

2. Find the folder of meshes

It is the relative location (from partition.sh) of the folder with the files:
Ra_795_fault*.vtu
This is `<folder of meshes>`

3. Run partition.sh

`partition.sh <number_of_partitions> <folder of meshes>`
e.g.
`partition.sh 40 rev0`


# Run the Example Simulation Model

Submit scripts are provided: 

|script|revision|Output algorithm|Filesystem|special|
| ------| ------ | ------ | ------ | ------ | 
| [1](rev2/480/1file_rp.sh)| rev2| HDF with 1file | LUSTRE | reference |
| [2](rev2/480/1file_ssd.sh)| rev2| HDF with 1file | SSD| |
| [3](rev2/480/1file_beegfs.sh)| rev2| HDF with 1file | BeeGFS| NOT WORKING! |
| [4](rev2/480/480files_rp.sh)| rev2| HDF with 480 files | LUSTRE|  |
| [5](rev2/480/480files_ssd.sh)| rev2| HDF with 480 files | SSD|  |
| [6](rev2/480/1file_nosecondary.sh)| rev2| HDF with 1 files | SSD| the secondary variable "darcy_velocity" is not written, to check influence of writing vectors |
| [7](rev2/480/vtufile_rp.sh)| rev2| VTU | LUSTRE | reference |
| [8](rev2/480/vtufile_ssd.sh)| rev2| VTU | SSD |  |
| [9](rev2/480/vtufile_beegfs.sh)| rev2| VTU | BeeGFs | NOT WORKING! |






[ogs_cube_ht_benchmark_rev0](rev0/24/ogs_cube_ht_benchmark_rev0.sh) and [ogs_cube_ht_benchmark_rev0_xdmf](rev0/24/ogs_cube_ht_benchmark_rev0_xdmf.sh).
